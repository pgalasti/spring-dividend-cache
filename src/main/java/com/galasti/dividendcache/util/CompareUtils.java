package com.galasti.dividendcache.util;

public class CompareUtils {

    public static Boolean isNull(Object obj) {
        return obj == null;
    }

    public static Boolean isNotNull(Object obj) {
        return !isNull(obj);
    }

    public static Boolean isNullOrEmpty(String str) {

        if(isNull(str)) {
            return true;
        }

        if(str.isEmpty()) {
            return true;
        }

        return false;
    }

    public static Boolean isNotNullOrEmpty(String str) {
        return !isNullOrEmpty(str);
    }
}
