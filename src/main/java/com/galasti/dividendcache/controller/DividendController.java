package com.galasti.dividendcache.controller;

import com.galasti.dividendcache.model.DividendDto;
import com.galasti.dividendcache.service.DividendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DividendController {

    private DividendService dividendService;

    @Autowired
    public DividendController(DividendService dividendService) {
        this.dividendService = dividendService;
    }

    @RequestMapping(value = "dividend/{ticker}", produces = "application/json")
    public ResponseEntity<DividendDto> fetchTicker(@PathVariable String ticker) {
        DividendDto dto = this.dividendService.fetchLatestDividend(ticker);
        return new ResponseEntity<DividendDto>(HttpStatus.OK).ok(dto);
    }

}
