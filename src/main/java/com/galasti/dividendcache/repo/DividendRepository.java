package com.galasti.dividendcache.repo;

import com.galasti.dividendcache.model.DividendDto;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DividendRepository extends CrudRepository<DividendDto, String> {
}
