package com.galasti.dividendcache;

import com.galasti.dividendcache.service.DividendHttpFetch;
import com.galasti.dividendcache.service.DividendHttpFetchImpl;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@PropertySource("classpath:url.properties")
public class DividendCacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(DividendCacheApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplateBuilder().build();
    }

}
