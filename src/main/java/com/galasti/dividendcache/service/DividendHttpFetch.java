package com.galasti.dividendcache.service;

import com.galasti.dividendcache.model.DividendDto;

public interface DividendHttpFetch {

    DividendDto fetchDividend(String ticker);
}
