package com.galasti.dividendcache.service;

import com.galasti.dividendcache.model.DividendDto;
import com.galasti.dividendcache.repo.DividendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;

@Service
public class CacheCheckService {

    private DividendRepository dividendRepository;

    @Autowired
    public CacheCheckService(DividendRepository dividendRepository) {
        this.dividendRepository = dividendRepository;
    }

    public Boolean isTickerValidInCache(String ticker) {
        Optional<DividendDto> record = this.dividendRepository.findById(ticker);
        if(!record.isPresent()) {
            return Boolean.FALSE;
        }

        DividendDto dto = record.get();
        if(dto.getLastUpdateDate().after(this.getYesterdayDate())) {
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }

    private Date getYesterdayDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.set(Calendar.DATE, -1);
        return cal.getTime();
    }
}
