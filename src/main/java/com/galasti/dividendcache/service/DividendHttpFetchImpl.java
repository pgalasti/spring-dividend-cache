package com.galasti.dividendcache.service;

import com.galasti.dividendcache.exception.HistoryNotFoundException;
import com.galasti.dividendcache.exception.InvalidTickerException;
import com.galasti.dividendcache.exception.TickerNotFoundException;
import com.galasti.dividendcache.model.DividendDto;
import com.galasti.dividendcache.util.CompareUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class DividendHttpFetchImpl implements DividendHttpFetch {

    private RestTemplate restTemplate;

    private String url;
    private String privateKey;

    @Autowired
    public DividendHttpFetchImpl(RestTemplate restTemplate, @Value("${api.url}") String url, @Value("${iex_private_key}") String privateKey) {
        this.restTemplate = restTemplate;
        this.url = url;
        this.privateKey = privateKey;
    }

    @Override
    public DividendDto fetchDividend(String ticker) {
        DividendDto dividends[] = null;

        if(CompareUtils.isNullOrEmpty(ticker)) {
            throw new InvalidTickerException("Ticker is invalid!");
        }

        ticker = ticker.toUpperCase();

        try {
            dividends = this.restTemplate.getForObject(
                    String.format(url, ticker, privateKey),
                    DividendDto[].class);
        } catch(HttpClientErrorException e) {
            throw new TickerNotFoundException(e);
        }

        if(dividends.length == 0) {
            throw new HistoryNotFoundException("Unable to find latest history for %s", ticker);
        }

        return dividends[0];
    }
}
