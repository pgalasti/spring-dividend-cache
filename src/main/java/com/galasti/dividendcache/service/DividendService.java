package com.galasti.dividendcache.service;

import com.galasti.dividendcache.model.DividendDto;

public interface DividendService {

    DividendDto fetchLatestDividend(String ticker);
}
