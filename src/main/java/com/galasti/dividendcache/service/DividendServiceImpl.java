package com.galasti.dividendcache.service;

import com.galasti.dividendcache.model.DividendDto;
import com.galasti.dividendcache.repo.DividendRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class DividendServiceImpl implements DividendService {

    private DividendRepository dividendRepository;
    private CacheCheckService cacheCheckService;
    private DividendHttpFetch dividendHttpFetch;

    @Autowired
    public DividendServiceImpl(DividendRepository dividendRepository, CacheCheckService cacheCheckService, DividendHttpFetch dividendHttpFetch) {
        this.dividendRepository = dividendRepository;
        this.cacheCheckService = cacheCheckService;
        this.dividendHttpFetch = dividendHttpFetch;
    }

    @Override
    public DividendDto fetchLatestDividend(String ticker) {

        if(cacheCheckService.isTickerValidInCache(ticker)) {
            return this.dividendRepository.findById(ticker).get();
        }

        DividendDto dto = this.dividendHttpFetch.fetchDividend(ticker);
        dto.setSymbol(ticker);
        dto.setLastUpdateDate(new Date());

        this.dividendRepository.save(dto);
        return dto;
    }
}
