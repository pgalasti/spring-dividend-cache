package com.galasti.dividendcache.exception;

public class TickerNotFoundException extends RuntimeException {

    public TickerNotFoundException() {
    }

    public TickerNotFoundException(String message) {
        super(message);
    }

    public TickerNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public TickerNotFoundException(Throwable cause) {
        super(cause);
    }
}
