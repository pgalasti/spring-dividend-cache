package com.galasti.dividendcache.exception;

public class InvalidTickerException extends RuntimeException {

    public InvalidTickerException() {
    }

    public InvalidTickerException(String message) {
        super(message);
    }

    public InvalidTickerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidTickerException(Throwable cause) {
        super(cause);
    }
}
