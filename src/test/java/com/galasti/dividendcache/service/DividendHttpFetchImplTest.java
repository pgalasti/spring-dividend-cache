package com.galasti.dividendcache.service;

import com.galasti.dividendcache.DividendCacheApplication;
import com.galasti.dividendcache.model.DividendDto;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DividendCacheApplication.class)
public class DividendHttpFetchImplTest {

    @Autowired
    private DividendHttpFetchImpl dividendHttpFetch;

    @Test
    public void getSPY() {
        DividendDto dto = dividendHttpFetch.fetchDividend("SPY");
        dto.getSymbol();
    }

}
