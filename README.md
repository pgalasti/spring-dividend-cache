# Spring Dividend Cache

### Description
Simple REST microservice caching latest dividend results. 

Several market APIs charge for ticker statistics based on REST usage. 

This service caches previous 24 hour dividend information to prevent exhausting
on a market provider API. 


### How To Use
Current implementation utilizes [IEX Cloud API](https://iexcloud.io/docs/api/) market provider.

Implementation requires IEX Cloud API private key provided as JVM variable: iex_private_key

### REST url
http://[service_url]:[port]/dividend/[ticker_symbol]

Example:

* localhost:8080/dividend/AAPL
